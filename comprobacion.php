<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
	
	<?php 
	
	try{
		$base=new PDO("mysql: host=localhost; dbname=testingdb" , "root", "");
		/*$base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);*/
		
		$query="SELECT * FROM users WHERE user_name= :login AND password= :pass";
		$resultado=$base->prepare($query);
		$login=htmlentities(addslashes($_POST["login"]));
		$pass=htmlentities(addslashes($_POST["pass"]));
		$resultado->bindValue(":login", $login);
		$resultado->bindValue(":pass", $pass);
		$resultado->execute();
		$numero_registro=$resultado->rowCount();
		
		if($numero_registro!=0){
			
			header('Location:index.php');
			
		}else{
			
			session_start();
			$_SESSION["user"]=$_POST["login"];
			header("Location:login.php");
			echo $numero_registro;
		}
		
	}catch(Exception $e){
		die("Error: " . $e->getMessage());
	}
	
	?>
</body>
</html>