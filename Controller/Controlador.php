<?
include_once("../clases/Users.php");
	
	class controllerUsers{
	
	private $user;
	
	public function __contruct(){
		$this->user = new Users();
	}
	public function index(){
		$resultado = $this->user->listar();
		return($resultado);
	}
	public function crear ($firs_name, $last_name, $user_name, $password, $create_at){
		$this->user->set("firs_name", $firs_name);
		$this->user->set("last_name", $last_name);
		$this->user->set("user_name", $user_name);
		$this->user->set("password", $password);
		$this->user->set("create_at", $create_at);
	}
	public function eliminar($id){
		$this->user->set("id",$id);
		$this->user->eliminar();
	}
	public function update($id){
		$this->user->set("id,$id");
		$this->user->visualizar();
		$this->user->update();
	}
		
}
?>